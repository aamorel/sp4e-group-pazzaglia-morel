'''
Homework 1 - Exercise 1
Scipy optimization

Classes
----------
    QuadraticFunction2D

Functions
-------
    minimize(functor: QuadraticFunction2D, method="BFGS") -> Tuple[float, np.ndarray]
        Minimize quadric function
    plot_opti(XX: np.ndarray, YY: np.ndarray, Z: np.ndarray,traj: np.ndarray, title: str) -> None
        Plot the quadratic function to optimize as a surface, and the optimizing trajectory
    main() -> None
        Executes Homework 1 - Exercise 1
'''

import scipy
import logging
import numpy as np
import matplotlib.pyplot as plt
from typing import Tuple

class QuadraticFunction2D():
    '''
    2D quadratic function class

    Attributes
    ----------
    A : np.ndarray
        The A matrix of the quadratic function
    b : np.ndarray
        The b vector of the quadratic function

    Methods
    -------
    __call__(x: np.ndarray):
        Evaluates the value of the quadratic function in x

    '''
    def __init__(self, A: np.ndarray, b: np.ndarray):
        '''
        Parameters of constructor
        ----------
            A: np.ndarray
                The A matrix of the quadratic function
            b: np.ndarray
                The b vector of the quadratic function
        '''

        # check if sizes are correct
        if (A.shape != (2, 2) or b.shape != (2,)):
            raise NameError("A shape should be (2, 2) and b shape (2,)")

        self.A = A
        self.b = b

    def __call__(self, x: np.ndarray) -> float:
        '''
        Evaluates the value of the quadratic function in x

        Parameters
        ----------
            x:  np.ndarray
                The coordinate where to evaluate the quadratic function

        Return
        ----------
            res: float
                The value of the quadratic function evaluated in x
        '''
        res = 0.5 * x.T@self.A@x - x.T@self.b
        return res

def minimize(functor: QuadraticFunction2D, method="BFGS") -> Tuple[float, np.ndarray]:
    '''
    Minimize quadric function

    Parameters
    ----------
        functor: QuadraticFunction2D
            A quadratic function functor
        method: str
            A method name

    Return
    ----------
        minimizer: float
            Resulting minimizer of quadratic function
        traj_3d: np.ndarray
            Minimization trajectory
    '''

    initial_guess = np.zeros(2)
    minimizer = initial_guess
    trajectories = [initial_guess]

    # callback for the optimizing trajectory
    def callback(x):
        trajectories.append(x)


    if (method == "BFGS"):
        res = scipy.optimize.minimize(functor, initial_guess, method="BFGS", callback=callback)
        if (res.success):
            minimizer = res.x
        else:
            raise NameError("Could not minimize with BFGS.")
    elif (method == "GMRES"):
        A = functor.A
        b = functor.b
        x, info = scipy.sparse.linalg.lgmres(A, b, initial_guess, atol=1e-9, callback=callback)
        if (info == 0):
            minimizer = x
        else:
            raise NameError("Could not minimize with GMRES.")
    else:
        raise NameError("Unknown method.")

    traj_3d = np.array([np.array([xy[0], xy[1], functor(xy)]) for xy in trajectories])
    return minimizer, traj_3d

def plot_opti(XX: np.ndarray, YY: np.ndarray, Z: np.ndarray,
            traj: np.ndarray, title: str):
    '''
    Plot the quadratic function to optimize as a surface, and the optimizing
    trajectory

    Parameters
    ----------
        XX: np.ndarray
            A mesh grid x vector
        YY: np.ndarray
            A mesh grid y vector
        Z: np.ndarray
            Z value of the surface
        traj: np.ndarray
            optimizing trajectory
        title: str
            A method name

    '''
    fig, ax = plt.subplots(subplot_kw={"projection": "3d"})

    # plot the trajectory
    ax.plot(traj[:, 0], traj[:, 1], traj[:, 2], 'r--', linewidth=1)
    ax.scatter(traj[:, 0], traj[:, 1], traj[:, 2], linewidth=1, color='red')

    # plot the surface
    ax.plot_surface(XX, YY, Z, linewidth=0, edgecolors=(0, 0, 0, 0), cmap='cividis', alpha=0.3)

    # plot the contours
    ax.contour3D(XX, YY, Z, 10, colors='black')

    # rotate the view
    ax.view_init(elev=60., azim=140)


    ax.set_title(title)
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlim([0, 70])

def main():
    '''
    Executes Homework 1 - Exercise 1
    '''
    # define parameters of the quadratic function
    A = np.array([[8, 1], [1, 3]])
    b = np.array([2, 4])

    # define the function
    func_to_minimize = QuadraticFunction2D(A, b)

    # test the function
    test_x = np.array([1, 2])
    test_val = func_to_minimize(test_x)
    # logging.info(test_val)

    # minimize with BFGS
    minimizer_BFGS, trajectories_BFGS = minimize(func_to_minimize, method="BFGS")
    logging.info(f"Minimizer found with BFGS: {minimizer_BFGS} with value of function: {func_to_minimize(minimizer_BFGS)}" )

    # minimize with GMRES
    minimizer_GMRES, trajectories_GMRES = minimize(func_to_minimize, method="GMRES")
    logging.info(f"Minimizer found with GMRES: {minimizer_GMRES} with value of function: {func_to_minimize(minimizer_GMRES)}")

    # make data
    X = np.arange(-3, 3, 0.03)
    Y = np.arange(-3, 3, 0.03)
    XX, YY = np.meshgrid(X, Y)
    Z = np.array([[func_to_minimize(np.array([x, y])) for x in X] for y in Y])

    # visualizing surfaces and optimizing trajectories
    plot_opti(XX, YY, Z, trajectories_BFGS, "Optimization with BFGS")
    plot_opti(XX, YY, Z, trajectories_GMRES, "Optimization with GMRES")

    # display figures
    plt.show()

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    main()
