'''
Homework 1 - Exercise 2
Implementation of the generalized minimal residual method (GMRES)

Call parameters
------
    a_dim_0: int
        Size of the first dimension of the A matrix
    a_dim_0: int
        Size of the second dimension of the A matrix
    ab_elements : list[float]
        List of the elements of the A matrix (a_dim_0 * a_dim_1)
        followed by the list of the elements of the b vector (a_dim_1)
    --solver : str, Optional
        Optionally specify the solver to be use for the GMRES iterative solver.
        Acceptable values are 'custom' (hand-made solver, default), 'linalg' (scipy.sparse.linalg.gmres),
        'optimize' (scipy.optimize.newton_krylov).
    --plot, Optional
        Optionally plot the trajectory of convergence of the algorithm.
        Only for 'custom' solver. Default = False.
    --no-plot, Optional
        Do not plot the trajectory of convergence of the algorithm. Default = True

Functions
-------
    vector_inner_product( vec_1: np.ndarray, vec_2: np.ndarray ) -> float
        Inner product between two vectors, using np.einsum
    matrix_vector_product( mat: np.ndarray, vec: np.ndarray ) -> np.ndarray
        Standard product between a matrix and a vector, using np.einsum
    matrix_product( mat_1: np.ndarray, mat_2: np.ndarray ) -> np.ndarray
        Standard product between two matrices, using np.einsum
    vector_norm( mat: np.ndarray ) -> np.ndarray
        Standard L-2 norm of a vector, using np.einsum
    custom_gmres( a_mat: np.ndarray, b_vec: np.ndarray, **kwargs, ) -> Tuple[List[np.ndarray], bool]
        Solve the linear system Ax = b using the GMRES method and return the vector x
    main() -> None
        Solution to Homework 1 - Exercise 2.
'''

import typing
import logging
import argparse
import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize as scipy_optimize
import scipy.sparse.linalg as scipy_linalg

from scipy.optimize._nonlin import NoConvergence
from optimizer import QuadraticFunction2D, plot_opti, minimize

# Standard algebraic operations with np.einsum

def vector_inner_product(
    vec_1: np.ndarray,
    vec_2: np.ndarray,
) -> float:
    '''
    Inner product between two vectors, using np.einsum

    Parameters
    ----------
        vec_1: np.ndarray
            An (m)-dimensional vector of floating point values
        vec_2: np.ndarray
            An (m)-dimensional vector of floating point values

    Return
    ----------
        inner_product: float
            Resulting inner product
    '''
    product = np.einsum('k,k->', vec_1, vec_2)
    return product

def matrix_vector_product(
    mat: np.ndarray,
    vec: np.ndarray,
) -> np.ndarray:
    '''
    Standard product between a matrix and a vector, using np.einsum

    Parameters
    ----------
        mat: np.ndarray
            An (n x m)-dimensional matrix of floating point values
        vec: np.ndarray
            An (m)-dimensional vector of floating point values

    Return
    ----------
        product: np.ndarray
            Resulting (n)-dimensional vector
    '''
    product = np.einsum('ij, j -> i', mat, vec)
    return product

def matrix_product(
    mat_1: np.ndarray,
    mat_2: np.ndarray,
) -> np.ndarray:
    '''
    Standard product between two matrices, using np.einsum

    Parameters
    ----------
        mat_1: np.ndarray
            An (n x m)-dimensional matrix of floating point values
        mat_2: np.ndarray
            An (m x k)-dimensional matrix of floating point values

    Return
    ----------
        mat_2: np.ndarray
            An (n x k)-dimensional matrix of floating point values
    '''
    product = np.einsum('ij, jk -> ik', mat_1, mat_2)
    return product

def vector_norm(
    mat: np.ndarray,
) -> np.ndarray:
    '''
    Standard L-2 norm of a vector, using np.einsum

    Parameters
    ----------
        vec: np.ndarray
            An (m)-dimensional vector of floating point values

    Return
    ----------
        vec_norm: float
            L-2 norm of the input vector
    '''
    vec_norm = np.sqrt( np.einsum('i, i ->', mat, mat) )
    return vec_norm

# GMRES solver

def custom_gmres(
    a_mat: np.ndarray,
    b_vec: np.ndarray,
    **kwargs,
) -> typing.Tuple[typing.List[np.ndarray], bool]:
    '''
    Solve the linear system Ax = b using the GMRES method and return the vector x

    Parameters
    ----------
        a_mat: np.ndarray
            An (n x m)-dimensional matrix of floating point values
        b_vec: np.ndarray
            An (m)-dimensional vector of floating point values
        x0: np.ndarray, Optional
            Initial guess for the solution. An (m)-dimensional vector of floating point values.
            Default to an array of zeros.
        tol: float, Optional
            Tolerance for the convergence of the iterative algorithm. Default to 1e-5.
        maxiter: int, Optional
            Maximum number of iterations for the iterative algorithm. Default to m.

    Return
    ----------
        traj_3d : list[ np.ndarray ]
            Successive approximations of the solution over the iterations and their corresponding error (Ax - b).
            An (m+1)-dimensional vector of floating point values
        converged: bool
            True if the algorithm found a solution satisfying the tolerance. False otherwise.
    '''

    # Consistency check
    assert a_mat.shape[0] == b_vec.shape[0]

    # Optional parameters
    tolerance = kwargs.pop('tol', 1e-5)
    x_vec_0   = kwargs.pop('x0', np.zeros(a_mat.shape[0]))
    max_iter  = min( kwargs.pop('maxiter', a_mat.shape[0]), a_mat.shape[0] )

    # Variables
    residual = b_vec - matrix_vector_product(a_mat, x_vec_0)

    residual_norm_mat    = np.zeros( (max_iter, len(b_vec) ) )
    residual_norm_mat[0] = residual / vector_norm(residual)

    residual_norm_vec    = np.zeros(max_iter + 1)
    residual_norm_vec[0] = vector_norm(residual)

    hess_mat = np.zeros((max_iter + 1, max_iter))

    # Algorithm
    x_vec      = x_vec_0
    error      = vector_norm( b_vec - matrix_vector_product(a_mat, x_vec) )
    traj_3D    = np.zeros( (max_iter+1, a_mat.shape[1]+1) )
    traj_3D[0] = np.append(x_vec, error)

    for iteration in range(max_iter):

        if error < tolerance:
            break

        y_vec = matrix_vector_product( a_mat, residual_norm_mat[iteration] )

        for j in range(iteration + 1):
            hess_mat[j, iteration] = vector_inner_product(residual_norm_mat[j], y_vec)
            y_vec = y_vec - hess_mat[j, iteration] * residual_norm_mat[j]

        y_norm = vector_norm(y_vec)
        hess_mat[iteration + 1, iteration] = y_norm

        if y_norm != 0 and iteration != max_iter - 1:
            residual_norm_mat[iteration + 1] = y_vec / y_norm

        hess_res_minimum = np.linalg.lstsq(hess_mat, residual_norm_vec, rcond=None)[0]

        x_vec = matrix_vector_product(residual_norm_mat.T, hess_res_minimum) + x_vec_0
        error = vector_norm( b_vec - matrix_vector_product(a_mat, x_vec) )

        traj_3D[iteration+1] = np.append(x_vec, error)

    converged = vector_norm( b_vec - matrix_vector_product(a_mat, x_vec) ) < tolerance
    return traj_3D, converged

# MAIN

def main():
    '''
    Solution of Exercise 2 of Homework 1.
    - Parse the arguments to define the solver type, dimensions of the A matrix, elements of
    the A matrix and the b vector, optional plotting.
    - Solve the linear system using the specified GMRES solver
    - Plot the results with the functions defined in Exercise 1
    '''

    # Parse solve, dimensions, input matrix and vector
    parser = argparse.ArgumentParser()

    parser.add_argument('a_dim_0',    type= int, help= 'The first dimension (m) of the A matrix')
    parser.add_argument('a_dim_1',    type= int, help= 'The second dimension (n) of the A matrix')
    parser.add_argument(
        'ab_elements',
        nargs= '+',
        type= float,
        help= 'The (m * n) elements of the A matrix followed by the (n) elements of the b vector'
    )

    parser.add_argument('--solver', type= str,  help= 'Optionally specify solver to be used')

    parser.add_argument('--no-plot', dest= 'plot', action= 'store_false')
    parser.add_argument(
        '--plot',
        default = False,
        action  = 'store_true',
        help    = 'Optionally plot the iteration steps'
    )

    args = parser.parse_args()

    a_dim_0     : int         = args.a_dim_0
    a_dim_1     : int         = args.a_dim_1
    ab_elements : list[float] = args.ab_elements

    assert len(ab_elements) == a_dim_0 * a_dim_1 + a_dim_1

    a_mat = np.array( ab_elements[:a_dim_0 * a_dim_1] ).reshape(a_dim_0, a_dim_1)
    b_vec = np.array( ab_elements[a_dim_0 * a_dim_1:])

    solver_str : str = args.solver
    if solver_str is not None:
        logging.info('Selected solver: %s', solver_str)
        assert solver_str in ['custom', 'linalg', 'optimize']

    plot_convergence = args.plot

    # # Reference
    # solver_str = 'optimize'
    # a_dim_0 = 2
    # a_dim_1 = 2
    # a_mat = np.array( [ [8, 1], [1, 3] ], dtype= float )
    # b_vec = np.array( [2, 4], dtype= float )

    # Solver
    x_vec_0   = np.zeros(a_dim_0)

    if solver_str == 'custom' or solver_str is None:
        trajectory_custom, converged = custom_gmres(
            a_mat = a_mat,
            b_vec = b_vec,
            x0    = x_vec_0,
        )
        x_vec = trajectory_custom[-1][:a_dim_1]

        if converged:
            logging.info('Custom GMRES: Successful convergence')
        else:
            raise NoConvergence('Custom GMRES: Convergence not achieved')

        if plot_convergence and a_dim_0 == 2 and a_dim_1 == 2:

            # Functor and grid
            func_to_minimize = QuadraticFunction2D(a_mat, b_vec)
            x0_vals = np.arange(-3, 3, 0.03)
            x1_vals = np.arange(-3, 3, 0.03)
            x0_grid, x1_grid = np.meshgrid(x0_vals, x1_vals)
            f_values = np.array(
                [
                    [ func_to_minimize(np.array([x, y])) for x in x0_vals ]
                    for y in x1_vals
                ]
            )

            # Scipy optimizer from exercise 1
            _, trajectory_BFGS = minimize(func_to_minimize, method="BFGS")

            # Visualizing surfaces and optimizing trajectories
            plot_opti(x0_grid, x1_grid, f_values, trajectory_custom, "Optimization with custom GMRES")
            plot_opti(x0_grid, x1_grid, f_values,   trajectory_BFGS, "Optimization with BFFS")
            plt.show()

    elif solver_str == 'linalg':
        x_vec, convergence_info = scipy_linalg.gmres(
            A  = a_mat,
            b  = b_vec,
            x0 = x_vec_0,
        )

        if convergence_info == 0:
            logging.info('Linalg GMRES: Successful convergence')
        else:
            raise NoConvergence('Linalg GMRES: Convergence not achieved')

    elif solver_str == 'optimize':
        try:
            x_vec = scipy_optimize.newton_krylov(
                F      = lambda x: (a_mat @ x - b_vec),
                xin    = x_vec_0,
                method = 'gmres',
            )
            logging.info('Optimize GMRES: Successful convergence')
        except NoConvergence:
            raise NoConvergence('Optimize GMRES: Convergence not achieved')

    logging.info(f"Minimizer found with GMRES: {x_vec}")

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    main()