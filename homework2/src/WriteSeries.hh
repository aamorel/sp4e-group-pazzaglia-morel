// -*- lsst-c++ -*
/*
 * This file is part of homework2.
 * Developed by Alessandro Pazzaglia and Aurelièn Morel for the SP4E course.
 */
#include <fstream>
#include <string>
#include "DumperSeries.hh"

/**
 * Class that prints series values to a file.
 */
class WriteSeries : public DumperSeries
{
public:
    WriteSeries(std::shared_ptr<Series> series, int frequency, int maxiter);

    /**
     * Prints the values of the series to the specified channel.
     *
     * @param os channel used for printing the series' values.
     */
    void dump(std::ostream &os) override;

    /**
     * Selects the separator used for writing the data to a file.
     *
     * @param separator character used to separate data points (available options are ' ' ',' '|').
     */
    void setSeparator(char separator = ' ');

protected:
    /// Separator for writing the data points to a file
    char separator;
    /// Name of the file where to write the data
    std::string filename;
};