// -*- lsst-c++ -*
/*
 * This file is part of homework2.
 * Developed by Alessandro Pazzaglia and Aurelièn Morel for the SP4E course.
 */
#include "ComputePi.hh"
#include <cmath>
#include <iostream>

double ComputePi::compute(unsigned int N)
{
    return sqrt(6.0 * Series::compute(N));
}

double ComputePi::computeIndex(unsigned int k)
{
    return 1.0 / (k * k);
}

double ComputePi::getAnalyticPrediction()
{
    return M_PI;
}