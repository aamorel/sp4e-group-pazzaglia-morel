// -*- lsst-c++ -*
/*
 * This file is part of homework2.
 * Developed by Alessandro Pazzaglia and Aurelièn Morel for the SP4E course.
 */
#include "RiemannIntegral.hh"
#include <iostream>

RiemannIntegral::RiemannIntegral(
    double a,
    double b,
    std::function<float(float)> f)
    : Series()
{
    this->a = a;
    this->b = b;
    this->f = f;
};

double RiemannIntegral::computeIndex(unsigned int k)
{
    return f(a + (k - 1) * dx);
}

double RiemannIntegral::compute(unsigned int N)
{
    // Different step with different N
    dx = (b - a) / N;

    // Reset value (changing N requires to re-start the series computation)
    current_index = 0;
    current_value = 0;
    return dx * Series::compute(N);
}