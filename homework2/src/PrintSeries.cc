// -*- lsst-c++ -*
/*
 * This file is part of homework2.
 * Developed by Alessandro Pazzaglia and Aurelièn Morel for the SP4E course.
 */
#include <iostream>
#include "PrintSeries.hh"

PrintSeries::PrintSeries(std::shared_ptr<Series> series, int frequency, int maxiter)
    : DumperSeries(series)
{
    this->frequency = frequency;
    this->maxiter = maxiter;
};

void PrintSeries::dump(std::ostream &os)
{

    double analytic_prediction = series->getAnalyticPrediction();

    if (std::isnan(analytic_prediction))
        os << "NO analytic prediction available" << std::endl;
    else
        os << "Analytic prediction: " << analytic_prediction << std::endl;

    for (int i = 1; i <= maxiter; i++)
    {
        if (i % frequency == 0)
        {
            os.precision(precision);
            os << "S(" << i << ") = " << series->compute(i) << std::endl;
        }
    }
};