// -*- lsst-c++ -*
/*
 * This file is part of homework2.
 * Developed by Alessandro Pazzaglia and Aurelièn Morel for the SP4E course.
 */
#pragma once

#include <functional>
#include "Series.hh"

/**
 * Class that computes the Riemann integral of a real function in real values.
 */
class RiemannIntegral : public Series
{
public:
    RiemannIntegral(double a, double b, std::function<float(float)> f);

    /**
     * Compute the value of the integral evaluated with N terms.
     *
     * @param N number of considered terms.
     * @return Value of the integral evaluated with N terms.
     */
    double compute(unsigned int N) override;

    /**
     * Computes k-th term of the numerical integration.
     *
     * @param k Index of the term to compute.
     * @return Value of the k-th term of the numerical integration.
     */
    double computeIndex(unsigned int k);

private:
    /// Integration step
    double dx;
    /// Lower limit of the integral
    double a;
    /// Upper limit of the integral
    double b;
    /// Function to be integrated
    std::function<float(float)> f;
};