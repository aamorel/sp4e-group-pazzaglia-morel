// -*- lsst-c++ -*
/*
 * This file is part of homework2.
 * Developed by Alessandro Pazzaglia and Aurelièn Morel for the SP4E course.
 */
#include "Series.hh"
#include <iostream>

double Series::compute(unsigned int N)
{
    if (current_index <= N)
    {
        N -= current_index;
    }
    else
    {
        current_index = 0;
        current_value = 0;
    }
    for (int k = 0; k < N; k++)
    {
        addTerm();
    }
    return current_value;
}

void Series::addTerm()
{
    current_index += 1;
    current_value += computeIndex(current_index);
}