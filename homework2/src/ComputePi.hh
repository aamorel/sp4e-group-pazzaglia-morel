// -*- lsst-c++ -*
/*
 * This file is part of homework2.
 * Developed by Alessandro Pazzaglia and Aurelièn Morel for the SP4E course.
 */
#pragma once

#include "Series.hh"

/**
 * Class that approximates the value of PI.
 */
class ComputePi : public Series
{
public:
    /**
     * Compute the value of the series evaluated with its first N terms.
     *
     * @param N number of considered terms.
     * @return Value of the series evaluated with its first N terms.
     */
    double compute(unsigned int N) override;

    /**
     * Get convergence value of the series.
     *
     * @return Asymptotic value of the series (PI).
     */
    double getAnalyticPrediction() override;

    /**
     * Computes k-th term of the series.
     *
     * @param k Index of the term to compute.
     * @return Value of the k-th term of the series 1/k^2.
     */
    double computeIndex(unsigned int k);
};