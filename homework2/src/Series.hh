// -*- lsst-c++ -*
/*
 * This file is part of homework2.
 * Developed by Alessandro Pazzaglia and Aurelièn Morel for the SP4E course.
 */
#pragma once
#include <math.h>

/**
 * A generic class for the computation of series' partial summation.
 */
class Series
{
public:
    /**
     * Compute the value of the series evaluated with its first N terms.
     *
     * @param N number of considered terms.
     * @return Value of the series evaluated with its first N terms.
     */
    virtual double compute(unsigned int N);

    /**
     * Get convergence value of the series (if convergent).
     *
     * @return Asymptotic value of the series, or nan if the series is divergent.
     */
    virtual double getAnalyticPrediction() { return std::nan(""); }

    /**
     * Computes k-th term of the series.
     *
     * @param k Index of the term to compute.
     * @return Value of the k-th term of the series.
     */
    virtual double computeIndex(unsigned int k) = 0;

    /**
     * Adds a term to the series and updates its value.
     */
    void addTerm();

    /// Last number of terms considered for the series evaluation
    unsigned int current_index = 0;
    /// Value of the last series evaluation
    double current_value = 0;
};