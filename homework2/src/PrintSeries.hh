// -*- lsst-c++ -*
/*
 * This file is part of homework2.
 * Developed by Alessandro Pazzaglia and Aurelièn Morel for the SP4E course.
 */
#include "DumperSeries.hh"

/**
 * Class that prints series values to a desired channel.
 */
class PrintSeries : public DumperSeries
{
public:
    PrintSeries(std::shared_ptr<Series> series, int frequency, int maxiter);

    /**
     * Prints the values of the series to the specified channel.
     *
     * @param os channel used for printing the series' values.
     */
    void dump(std::ostream &os = std::cout) override;
};