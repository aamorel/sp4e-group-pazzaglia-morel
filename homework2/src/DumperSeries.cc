// -*- lsst-c++ -*
/*
 * This file is part of homework2.
 * Developed by Alessandro Pazzaglia and Aurelièn Morel for the SP4E course.
 */
#include "DumperSeries.hh"

DumperSeries::DumperSeries(std::shared_ptr<Series> series)
{
    this->series = series;
    this->precision = 4;
}

void DumperSeries::setPrecision(unsigned int precision)
{
    this->precision = precision;
}
