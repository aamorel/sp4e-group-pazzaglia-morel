// -*- lsst-c++ -*
/*
 * This file is part of homework2.
 * Developed by Alessandro Pazzaglia and Aurelièn Morel for the SP4E course.
 */
#include <iostream>
#include <sstream>
#include <memory>
#include <unordered_set>
#include <iomanip>
#include "ComputeArithmetic.hh"
#include "ComputePi.hh"
#include "RiemannIntegral.hh"
#include "PrintSeries.hh"
#include "WriteSeries.hh"

/// Function to compute the minimum number of steps to reach convergence of integral
void stepsForConvergence(double expected_result, RiemannIntegral &integral)
{
    int N_min = 1;
    double error = INFINITY;
    while (error > 0.01)
    {
        N_min++;
        error = abs(integral.compute(N_min) - expected_result);
    }
    std::cout << std::endl
              << std::scientific
              << std::setprecision(6)
              << "Minimum N = " << N_min << " Error = " << error << std::endl;
}

int main(int argc, char **argv)
{

    // Parse input (frequency, maxiter, action)
    if (argc != 4)
    {
        std::cerr << "Wrong input: expected 3 arguments (frequency, maxiter, action). "
                  << "Received " << argc - 1 << std::endl;
        exit(0);
    }

    std::stringstream sstr;
    for (int i = 1; i < argc; ++i)
        sstr << argv[i] << " ";

    int frequency;
    sstr >> frequency;

    int maxiter;
    sstr >> maxiter;

    char action;
    sstr >> action;

    // exercise 2
    int typeOfSeries = 0;
    std::cout << std::endl
              << "EXERCISE 2" << std::endl;
    std::cout << "Choose type of series to instantiate (0 for arithmetic, 1 for pi): ";
    std::cin >> typeOfSeries;

    std::shared_ptr<Series> chosenSeries;
    if (typeOfSeries == 0)
    {
        auto series = std::make_shared<ComputeArithmetic>();
        chosenSeries = series;
    }
    else if (typeOfSeries == 1)
    {
        auto series = std::make_shared<ComputePi>();
        chosenSeries = series;
    }
    else
    {
        std::cerr << "Wrong input";
        exit(0);
    }

    std::cout << "Result of chosen series: " << chosenSeries->compute(maxiter) << std::endl;

    // exercise 3
    std::cout << std::endl
              << "EXERCISE 3" << std::endl;

    // Prepare dumper
    std::shared_ptr<DumperSeries> chosenDumper;
    switch (action)
    {
    case 'p':
    {
        auto dumper = std::make_shared<PrintSeries>(chosenSeries, frequency, maxiter);
        chosenDumper = dumper;
    }
    break;

    case 'w':
    {
        auto dumper = std::make_shared<WriteSeries>(chosenSeries, frequency, maxiter);

        std::string fileType;
        std::cout << "Input desired file type (available options are 'txt','csv','psv'): ";
        std::cin >> fileType;

        char separator;
        if (fileType == "txt")
            separator = ' ';
        else if (fileType == "csv")
            separator = ',';
        else if (fileType == "psv")
            separator = '|';
        else
        {
            std::cerr << "Wrong input, file type should be in ['txt','csv','psv'], inserted: " << fileType << std::endl;
            exit(0);
        }

        dumper->setSeparator(separator);
        chosenDumper = dumper;
    }
    break;
    default:
        std::cerr << "Wrong input, action should be in ['p', 'w'], inserted: " << action << std::endl;
        exit(0);
    }
    chosenDumper->dump();

    // exercise 4 + 5
    std::cout << std::endl
              << "EXERCISE 4 + 5" << std::endl;

    PrintSeries series_dumper(chosenSeries, frequency, maxiter);
    series_dumper.setPrecision(6);
    series_dumper.dump();

    std::ofstream fout("output/dump_from_operator.txt");
    fout << series_dumper;
    fout.close();

    // exercise 6
    std::cout << std::endl
              << "EXERCISE 6" << std::endl;

    float a, b;
    std::string f_str;

    // Get user input
    std::cout << "Input function to integrate (available choices are 'cos', 'sin', 'cube'): ";
    std::cin >> f_str;

    std::unordered_set<std::string> f_choices = {"cos", "sin", "cube"};
    if (f_choices.find(f_str) == f_choices.end())
    {
        std::cerr << "Wrong input, function should be in ['cos', 'sin', 'cube'], inserted: " << f_str << std::endl;
        exit(0);
    }

    std::cout << "Input lower limit of the integral: ";
    std::cin >> a;

    std::cout << "Input upper limit of the integral: ";
    std::cin >> b;

    // Select function
    std::function<float(float)> f;

    if (f_str == "sin")
        f = [](double x)
        { return sin(x); };
    else if (f_str == "cos")
        f = [](double x)
        { return cos(x); };
    else if (f_str == "cube")
        f = [](double x)
        { return x * x * x; };

    RiemannIntegral integralSeries(a, b, f);

    std::cout << "Integral_{" << a << "}^{" << b << "}( " << f_str << "(x)dx ) = "
              << integralSeries.compute(100) << std::endl;

    // Compute integral value of point 3 (expected result is 0)
    RiemannIntegral integCube(0, 1, [](double x)
                              { return x * x * x; });
    RiemannIntegral integCos(0, M_PI, [](double x)
                             { return cos(x); });
    RiemannIntegral integSin(0, M_PI_2, [](double x)
                             { return sin(x); });

    stepsForConvergence(0.25, integCube);
    stepsForConvergence(0.00, integCos);
    stepsForConvergence(1.00, integSin);

    return 0;
}