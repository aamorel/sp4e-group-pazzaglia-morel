// -*- lsst-c++ -*
/*
 * This file is part of homework2.
 * Developed by Alessandro Pazzaglia and Aurelièn Morel for the SP4E course.
 */
#pragma once
#include <iostream>
#include <memory>
#include "Series.hh"

/**
 * A generic class for printing the values of a series.
 */
class DumperSeries
{
public:
    DumperSeries(std::shared_ptr<Series> series);

    /**
     * Prints the values of the series, either to screen or to a file.
     *
     * @param os channel used for printing the series' values.
     */
    virtual void dump(std::ostream &os = std::cout) = 0;

    /**
     * Sets the decimal precision for the printing of the dump method.
     *
     * @param precision number of decimal places to be used for printing.
     */
    virtual void setPrecision(unsigned int precision);

protected:
    /// Smart pointer to the series object used for dump
    std::shared_ptr<Series> series;
    /// Interval of summation terms between the print statements
    int frequency;
    /// Maximum number of series terms
    int maxiter;
    /// Number of decimal places considered to print the series' values
    unsigned int precision;
};

inline std::ostream &operator<<(std::ostream &stream, DumperSeries &_this)
{
    _this.dump(stream);
    return stream;
}