// -*- lsst-c++ -*
/*
 * This file is part of homework2.
 * Developed by Alessandro Pazzaglia and Aurelièn Morel for the SP4E course.
 */
#include "WriteSeries.hh"

WriteSeries::WriteSeries(std::shared_ptr<Series> series, int frequency, int maxiter)
    : DumperSeries(series)
{
    this->frequency = frequency;
    this->maxiter = maxiter;

    this->separator = ' ';
    this->filename = "output/series_convergence.txt";
};

void WriteSeries::dump(std::ostream &os)
{

    std::ofstream fout(filename);
    fout.precision(precision);

    double analytic_prediction = series->getAnalyticPrediction();

    fout << analytic_prediction << separator;

    for (int i = 1; i <= maxiter; i++)
    {
        if (i % frequency == 0)
        {
            fout << i << separator;
            fout << series->compute(i) << separator;
        }
    }

    fout.close();
};

void WriteSeries::setSeparator(char separator)
{

    std::string filename;
    switch (separator)
    {
    case ',':
        filename = "output/series_convergence.csv";
        break;
    case '|':
        filename = "output/series_convergence.psv";
        break;
    case '.':
        filename = "output/series_convergence.txt";
        break;
    default:
        filename = "output/series_convergence.txt";
    }

    this->separator = separator;
    this->filename = filename;
}
