// -*- lsst-c++ -*
/*
 * This file is part of homework2.
 * Developed by Alessandro Pazzaglia and Aurelièn Morel for the SP4E course.
 */
#include "ComputeArithmetic.hh"

double ComputeArithmetic::computeIndex(unsigned int k)
{
    return 1.0 * k;
}