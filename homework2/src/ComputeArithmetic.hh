// -*- lsst-c++ -*
/*
 * This file is part of homework2.
 * Developed by Alessandro Pazzaglia and Aurelièn Morel for the SP4E course.
 */
#pragma once

#include "Series.hh"

/**
 * Class to compute the sum of the first N natural numbers.
 */
class ComputeArithmetic : public Series
{
public:
    /**
     * Computes k-th term of the series.
     *
     * @param k Index of the term to compute.
     * @return Value of the k-th term of the series (k).
     */
    double computeIndex(unsigned int k);
};