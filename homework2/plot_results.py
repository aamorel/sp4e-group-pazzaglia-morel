"""
Homework 2 - Exercise 3
Module to plot the series elements printed to a file

Call parameters
------
    filetype: str
        The filetype for the series_convergence file
"""
import argparse
import matplotlib.pyplot as plt

def main():
    '''
    Reads data from a file and plots it together with its convergence value (if not NAN)
    '''

    parser = argparse.ArgumentParser()
    parser.add_argument('filetype', type= str, help= 'The filetype for the series_convergence file')

    args  = parser.parse_args()
    filetype : int = args.filetype

    if filetype == 'txt':
        separator = ' '
        filename = "output/series_convergence.txt"
    elif filetype == 'csv':
        separator = ','
        filename = "output/series_convergence.csv"
    elif filetype == 'psv':
        separator = '|'
        filename = "output/series_convergence.psv"

    file = open(filename, 'r')
    file_content = file.read()

    file_values = file_content.split(separator)[:-1]

    prediction = file_values[0]
    n_values = file_values[1::2]
    s_values = file_values[2::2]

    plt.figure()
    plt.plot(n_values, s_values, 'r*', label= 'series')

    if prediction != 'nan':
        plt.plot(n_values, [prediction]*len(n_values), 'g', marker='o', label='prediction')
    plt.xlabel('Terms of the series [#]')
    plt.ylabel('Value of the series')
    plt.legend()

    plt.show()


if __name__ == '__main__':
    main()