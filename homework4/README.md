# SP4E-group-Pazzaglia-Morel

Repository to store the solutions to the homeworks from the course SP4E (Autumn 2022).
Alessandro Pazzaglia - Aurélien Morel

# Homework 4

## Requirements

Python environment with numpy, matplotlib

C++ compiler

CMake minimum version 3.16.3

## Instructions to run the program

```bash
# build the main program
mkdir build
cd build
cmake ../
make

# make dumps folder
mkdir dumps

# run trajectory optimization
python trajectory_optimization.py
```

## Questions

1.2. The overloaded *createSimulation* method allows to give a functor as an additional argument. The functor helps the user to easily create the different *Compute* objects that will take part in the simulation.

2.2. In order to ensure that *Compute* object types are correctly managed by the Python bindings, we use *std::shared_ptr* to wrap the objects. 





