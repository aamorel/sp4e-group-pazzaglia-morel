import csv
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import animation
from typing import List

# The only admitted parameters as we only have the ground truth for them
NSTEPS        = 365
FREQ          = 1
FILENAME      = 'init.csv'
PARTICLE_TYPE = 'planet'
TIMESTEP      = 1
PLANET_NAMES  = ('sun', 'mercury','neptune','jupiter','uranus','mars','earth','venus','saturn')


def readPlanetParameters(planet_name: str, filename: str = None) -> np.ndarray:
    ''' Get all the planet parameters from the specified csv file '''

    with open(filename) as file:
        csvreader  = csv.reader(file)

        for planet_pars in csvreader:
            if not planet_pars[0] or planet_pars[0][0] == '#':
                continue

            planet_pars_list = planet_pars[0].split()

            if planet_pars_list[-1] == planet_name:
                # Return X, Y, Z position of the planet
                positions  = np.array(  [ float( planet_pars_list[i] ) for i in range(0, 3) ] )
                velocities = np.array(  [ float( planet_pars_list[i] ) for i in range(3, 6) ] )
                forces     = np.array(  [ float( planet_pars_list[i] ) for i in range(6, 9) ] )
                mass       = float( planet_pars_list[9] )
                name       = planet_pars_list[-1]
                return positions, velocities, forces, mass, name

    ValueError(f'{planet_name} is not among the listed planets')

def readPlanetPosition(
    planet_name: str,
    directory: str,
    step: int,
    filename: str = None
) -> np.ndarray:
    ''' Get the planet position from csv file for the specified step of the simulation '''

    filename = f'{directory}/step-{step:04}.csv' if filename is None else filename

    with open(filename) as file:
        csvreader  = csv.reader(file)

        for planet_pars in csvreader:
            if not planet_pars[0] or planet_pars[0][0] == '#':
                continue

            planet_pars_list = planet_pars[0].split()

            if planet_pars_list[-1] == planet_name:
                # Return X, Y, Z position of the planet
                return np.array(  [ float( planet_pars_list[i] ) for i in range(0,3) ] )

    ValueError(f'{planet_name} is not among the listed planets')

def readPositions(planet_name: str, directory: str) -> np.ndarray:
    ''' Reads the trajectory of a given planet and makes a numpy array out of it '''

    planet_positions = np.vstack(
        [ readPlanetPosition(planet_name, directory, step) for step in range(NSTEPS) ],
    )
    return planet_positions

def computeError(positions: np.ndarray, positions_ref: np.ndarray) -> float:
    ''' Computes the MSE error between the computed and reference trajectory '''
    return np.linalg.norm( np.linalg.norm(positions - positions_ref, axis=1) )

def plot_trajectories(folder: str, masses: List[str],  colors: np.ndarray):
    ''' Plots the trajectories of the planets in 2D and in 3D'''

    n_planets = len(masses)
    positions_all_planets =  np.array(
        [ readPositions(planet_name, folder) for planet_name in PLANET_NAMES ]
    )

    # Plot projections on the XY plane
    fig1 = plt.figure(f'{folder} folder - Planets trajectories 2D')

    plt.scatter(
        positions_all_planets[:, 0, 0],
        positions_all_planets[:, 0, 1],
        s = np.clip( masses, 100, 1000),
        c = colors
    )
    for planet_ind in range(n_planets):

        planet_trajectory = positions_all_planets[planet_ind]
        plt.plot(
            planet_trajectory[:, 0],
            planet_trajectory[:, 1],
            color = colors[planet_ind],
            label = PLANET_NAMES[planet_ind]
        )
    plt.legend()

    # Animation of the 3D trajectories
    fig2 = plt.figure(f'{folder} folder - Planets trajectories 3D - Zoomed')
    ax2 = plt.axes(projection='3d', xlim= [-1,1], ylim= [-1, 1], zlim= [-1,1])

    for planet_ind in range(n_planets):

        planet_trajectory = positions_all_planets[planet_ind]
        ax2.plot3D(
            planet_trajectory[:, 0],
            planet_trajectory[:, 1],
            planet_trajectory[:, 2],
            color = colors[planet_ind]
        )

    scat = ax2.scatter3D(
        positions_all_planets[:, 0, 0],
        positions_all_planets[:, 0, 1],
        positions_all_planets[:, 0, 2],
        s = np.clip( masses, 100, 1000),
        c = colors
    )

    def animate(i):
        scat._offsets3d = (
            positions_all_planets[:, i, 0],
            positions_all_planets[:, i, 1],
            positions_all_planets[:, i, 2],
        )

        ax2.set_title(f'Step: {i*FREQ} / {NSTEPS}')
        return [ax2, scat]

    # Call the animator
    anim = animation.FuncAnimation(
        fig2,
        animate,
        frames    = NSTEPS  // FREQ,
        interval  = 20,
    )

    return anim

def plot_all(errors: List[float]):
    ''' Plot integral errors and planets trajectories '''

    n_planets = len(PLANET_NAMES)
    rng    = np.random.default_rng(2023)
    colors = rng.random(size= (n_planets, 3))
    colors[0] = [1, 0, 0]

    # Integral errors
    # NOTE: Quantitative comparison between the reference and simulation
    fig = plt.figure('Integral errors')

    plt.bar(
        PLANET_NAMES,
        errors,
        color = colors,
        width = 0.4
    )

    plt.xlabel("Planet")
    plt.ylabel("Error")
    plt.title("Integral error")

    # Trajectories
    # NOTE: Qualitative comparison between the reference and simulation
    # Get parameters and trajectories
    parameters_planets = [
        readPlanetParameters(planet_name, filename= 'init.csv')
        for planet_name in PLANET_NAMES
    ]
    masses = [ parameters_planets[i][3] for i in range(n_planets) ]

    sim_plots = plot_trajectories(folder= 'trajectories', masses= masses, colors= colors)
    ref_plots = plot_trajectories(folder= 'dumps', masses= masses, colors= colors)
    plt.show()

def main():
    ''' Computes the error in the mercury trajectory and plots the planets positions '''

    # Compute integral errors
    errors = []
    for planet_name in PLANET_NAMES:
        positions_simulated = readPositions(planet_name, 'dumps')
        positions_reference = readPositions(planet_name, 'trajectories')
        positions_error     = computeError(positions_simulated, positions_reference)
        errors.append(positions_error)
        print(f'MSE {planet_name} : {positions_error}')

    plot_all(errors)

if __name__ == '__main__':
    main()