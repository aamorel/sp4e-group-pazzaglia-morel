import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import fmin
from main import main as simulation_run
from trajectory_analysis import computeError, readPositions

def generate_input(
        scale: float,
        planet_name: str,
        input_filename: str,
        output_filename: str
) -> None:
    '''
    Generates an input file from a given input file
    but by scaling velocity of a given planet
    '''
    # Open both files
    with open(input_filename) as f_in, open(output_filename, 'w') as f_out:
        # Write header unchanged
        header = f_in.readline()
        f_out.write(header)

        # Transform the rest of the lines
        found_planet = False
        for line in f_in:
            components = line.split(' ')
            if (components[-2] == planet_name):
                components[3] = str(float(components[3]) * scale)
                components[4] = str(float(components[4]) * scale)
                components[5] = str(float(components[5]) * scale)
                found_planet = True
                new_line = ' '.join(components)
                f_out.write(new_line)
            else:
                f_out.write(line)
        if (not found_planet):
            raise Exception('Planet name does not match.')

def launch_particles(input_file: str, nb_steps: int, freq: int) -> None:
    ''' Launches the particle code on an provided input '''
    simulation_run(
        nsteps=nb_steps,
        freq=freq,
        filename=input_file,
        particle_type='planet',
        timestep=1
    )

def get_current_error(planet_name: str) -> float:
    ''' Return the current error in the approximation for a given planet'''
    positions_simulated = readPositions(planet_name, 'dumps')
    positions_reference = readPositions(planet_name, 'trajectories')
    return computeError(positions_simulated, positions_reference)

def run_and_compute_error(
        scale: float,
        planet_name: str,
        input_file: str,
        nb_steps: int,
        freq: int
) -> float:
    ''' Gives the error for a given scaling velocity factor of a given planet '''
    generate_input(scale, planet_name, input_file, 'scaled_init.csv')
    launch_particles('scaled_init.csv', nb_steps, freq)
    return get_current_error(planet_name)

def function_to_optimize(scale, convergence_history: list):
    ''' Function to be optimized, also used to save the convergence history '''
    error = run_and_compute_error(
            scale[0],
            planet_name = 'mercury',
            input_file       = 'init.csv',
            nb_steps    = 365,
            freq        = 1,
    )
    convergence_history.append( [scale[0], error] )
    return error

def main():
    ''' Main '''

    # Run optimization with provided function (fmin provides x0 a an array )
    convergence_history = []
    fmin(
        func = function_to_optimize,
        x0   = np.array([1]),
        args = (convergence_history,)
    )

    convergence_history = np.array(convergence_history)
    convergence_history_sorted = convergence_history[convergence_history[:, 0].argsort()]

    # Plotting
    fig, ax = plt.subplots()
    ax.plot(
        convergence_history_sorted[:, 0],
        convergence_history_sorted[:, 1],
        marker= 'o',
        linestyle='--'
    )
    ax.set_yscale('log')
    ax.set_xlabel('Scaling')
    ax.set_ylabel('Error')
    ax.set_title('Scaling-Error relationship')
    ax.grid()

    plt.show()

if __name__ == '__main__':
    main()
