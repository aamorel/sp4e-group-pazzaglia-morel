#include <pybind11/pybind11.h>

namespace py = pybind11;

#include "compute_temperature.hh"
#include "compute_verlet_integration.hh"
#include "csv_writer.hh"
#include "material_points_factory.hh"
#include "ping_pong_balls_factory.hh"
#include "planets_factory.hh"
#include "compute_gravity.hh"

PYBIND11_MODULE(pypart, m) {

  m.doc() = "Python bindings of the Particles project";

  // bind the routines here
  py::class_<ParticlesFactoryInterface>(m, "ParticlesFactoryInterface")
    .def_static("getInstance", &ParticlesFactoryInterface::getInstance, py::return_value_policy::reference)
    .def_property_readonly("system_evolution", &ParticlesFactoryInterface::getSystemEvolution, py::return_value_policy::reference)
    .def("createSimulation", py::overload_cast<const std::string&, Real, py::function>(&ParticlesFactoryInterface::createSimulation<py::function>), py::return_value_policy::reference);

  py::class_<PingPongBallsFactory, ParticlesFactoryInterface>(m, "PingPongBallsFactory")
    .def_static("getInstance", &PingPongBallsFactory::getInstance, py::return_value_policy::reference);

  py::class_<PlanetsFactory, ParticlesFactoryInterface>(m, "PlanetsFactory")
    .def_static("getInstance", &PlanetsFactory::getInstance, py::return_value_policy::reference);

  py::class_<MaterialPointsFactory, ParticlesFactoryInterface>(m, "MaterialPointsFactory")
    .def_static("getInstance", &MaterialPointsFactory::getInstance, py::return_value_policy::reference);

  py::class_<System>(m, "System");

  py::class_<Compute, std::shared_ptr<Compute>>(m, "Compute");

  py::class_<ComputeInteraction, Compute, std::shared_ptr<ComputeInteraction>>(m, "ComputeInteraction")
    .def("applyOnPairs", &ComputeInteraction::applyOnPairs<py::function>);

  py::class_<ComputeVerletIntegration, Compute, std::shared_ptr<ComputeVerletIntegration>>(m, "ComputeVerletIntegration")
    .def(py::init<Real>())
    .def("addInteraction", &ComputeVerletIntegration::addInteraction);

  py::class_<ComputeGravity, ComputeInteraction, std::shared_ptr<ComputeGravity>>(m, "ComputeGravity")
    .def(py::init<>())
    .def("setG", &ComputeGravity::setG);

  py::class_<ComputeTemperature, Compute, std::shared_ptr<ComputeTemperature>>(m, "ComputeTemperature")
    .def(py::init<>())
    .def_property("conductivity", &ComputeTemperature::getConductivity, [](ComputeTemperature& self, Real val){ self.getConductivity() = val; })
    .def_property("capacity",     &ComputeTemperature::getCapacity,     [](ComputeTemperature& self, Real val){ self.getCapacity() = val;     })
    .def_property("density",      &ComputeTemperature::getDensity,      [](ComputeTemperature& self, Real val){ self.getDensity() = val;      })
    .def_property("L",            &ComputeTemperature::getL,            [](ComputeTemperature& self, Real val){ self.getL() = val;            })
    .def_property("deltat",       &ComputeTemperature::getDeltat,       [](ComputeTemperature& self, Real val){ self.getDeltat() = val;       });

  py::class_<SystemEvolution>(m, "SystemEvolution")
    .def("getSystem",   &SystemEvolution::getSystem, py::return_value_policy::reference)
    .def("evolve",      &SystemEvolution::evolve)
    .def("setNSteps",   &SystemEvolution::setNSteps)
    .def("setDumpFreq", &SystemEvolution::setDumpFreq)
    .def("addCompute",  &SystemEvolution::addCompute);



  py::class_<CsvWriter>(m, "CsvWriter")
    .def(py::init<const std::string&>())
    .def("write", &CsvWriter::write);
}
