/**
 * Functions for the computation of the FFT with the FFTW3 library
 */
#ifndef FFT_HH
#define FFT_HH
/* ------------------------------------------------------ */
#include "matrix.hh"
#include "my_types.hh"
#include <fftw3.h>
/* ------------------------------------------------------ */

struct FFT {

  // Return the Fast Fourier Transform of the input matrix
  static Matrix<complex> transform(Matrix<complex>& m);
  // Return the InverseFast Fourier Transform of the input matrix
  static Matrix<complex> itransform(Matrix<complex>& m);
  // Compute the Fourier coordinates
  static Matrix<std::pair<double, double>> computeFrequencies(int size, double spacing=1);
};

/* ------------------------------------------------------ */

inline Matrix<complex> FFT::transform(Matrix<complex>& m_in)
{

  // Allocate input and output arrays of type fftw_complex (double[2])
  int size  = m_in.rows();
  int size2 = size * size;

  fftw_complex* in  = fftw_alloc_complex(sizeof(fftw_complex) * size2);
  fftw_complex* out = fftw_alloc_complex(sizeof(fftw_complex) * size2);

  // Inizialize array
  for (int i = 0; i<size; i++){
    for (int j = 0; j<size; j++){
      in[i*size + j][0] = std::real(m_in(j, i));
      in[i*size + j][1] = std::imag(m_in(j, i));
    }
  }

  // Create plan
  fftw_plan p;
  p = fftw_plan_dft_2d( size, size, in, out, FFTW_FORWARD, FFTW_ESTIMATE);

  // Compute transform
  fftw_execute(p);

  // Output matrix
  Matrix<complex> m_out(size);

  for (int i = 0; i<size; i++){
    for (int j = 0; j<size; j++){
      complex elem( out[i*size + j][0], out[i*size + j][1]);
      m_out(j, i) = elem;
    }
  }

  // Free resources
  fftw_destroy_plan(p);

  // fftw_free(in);
  fftw_free(out);

  return m_out;
}

/* ------------------------------------------------------ */

inline Matrix<complex> FFT::itransform(Matrix<complex>& m_in) {
  // Allocate input and output arrays of type fftw_complex (double[2])
  int size  = m_in.rows();
  int size2 = size * size;

  fftw_complex* in  = fftw_alloc_complex(sizeof(fftw_complex) * size2);
  fftw_complex* out = fftw_alloc_complex(sizeof(fftw_complex) * size2);

  // Inizialize array
  for (int i = 0; i<size; i++){
    for (int j = 0; j<size; j++)
    {
      in[i*size + j][0] = std::real(m_in(j, i));
      in[i*size + j][1] = std::imag(m_in(j, i));
    }
  }

  // Create plan
  fftw_plan p;
  p = fftw_plan_dft_2d( size, size, in, out, FFTW_BACKWARD, FFTW_ESTIMATE );

  // Compute transform
  fftw_execute(p);

  // Output matrix and normalization
  Matrix<complex> m_out(size);

  for (int i = 0; i<size; i++){
    for (int j = 0; j<size; j++){
      complex elem( out[i*size + j][0] / size2, out[i*size + j][1] / size2);
      m_out(j, i) = elem;
    }
  }

  // Free resources and return
  fftw_destroy_plan(p);
  fftw_free(in);
  fftw_free(out);

  return m_out;
}

inline double freqIfSizeEven(int index, int size, double spacing) {
  double freq;
  if (index <= size/2 -1) {
    freq = index;
  } else {
    freq = index - size;
  }
  return freq / (double(size) * spacing);
}

inline double freqIfSizeOdd(int index, int size, double spacing) {
  double freq;
  if (index <= (size-1)/2 ) {
    freq = index;
  } else {
    freq = index - size;
  }
  return freq / (double(size) * spacing);
}

/* ------------------------------------------------------ */

inline Matrix<std::pair<double, double>> FFT::computeFrequencies(int size, double spacing) {
  Matrix<std::pair<double, double>> a(size);
  for (auto&& entry : index(a)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    if (size % 2 == 0) {
      // Size is even
      val.first = freqIfSizeEven(i, size, spacing);
      val.second = freqIfSizeEven(j, size, spacing);
    } else {
      val.first = freqIfSizeOdd(i, size, spacing);
      val.second = freqIfSizeOdd(j, size, spacing);
    }
  }
  return a;
}

#endif  // FFT_HH
