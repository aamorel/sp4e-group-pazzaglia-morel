#include "fft.hh"
#include "my_types.hh"
#include <gtest/gtest.h>

/*****************************************************************/
// Test the Fast Fourier Transform
TEST(FFT, transform) {
  UInt N = 512;
  Matrix<complex> m(N);

  Real k = 2 * M_PI / N;
  for (auto&& entry : index(m)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    val = cos(k * i);
  }

  Matrix<complex> res = FFT::transform(m);

  for (auto&& entry : index(res)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);

    if (i == 1 && j == 0)
      ASSERT_NEAR(std::abs(val), N * N / 2, 1e-10);
    else if (i == N - 1 && j == 0)
      ASSERT_NEAR(std::abs(val), N * N / 2, 1e-10);
    else
      ASSERT_NEAR(std::abs(val), 0, 1e-10);
  }
}

/*****************************************************************/
// Test the Inverse Fast Fourier Transform
TEST(FFT, inverse_transform) {
  UInt N = 512;
  Matrix<complex> m(N);

  // Known FFT
  for (auto&& entry : index(m)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);

    if (i == 1 && j == 0)
      val =  N * N / 2;
    else if (i == N - 1 && j == 0)
      val =  N * N / 2;
    else
      val = 0;
  }

  // Inverse transform and test
  Matrix<complex> res = FFT::itransform(m);

  Real k = 2 * M_PI / N;
  for (auto&& entry : index(res)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);

    ASSERT_NEAR(std::real(val), cos(k * i), 1e-10);
    ASSERT_NEAR(std::imag(val), 0, 1e-10);
  }

}

/*****************************************************************/
// Test the computeFrequencies function and compare it to numpy.fft.fftfreqs
TEST(FFT, compute_frequencies) {
  UInt N = 5;
  double d = 100;

  // Result from numpy.fft.fftfreqs with N=5 and d=100
  double freq_np[N][N][2] = {
      { {0.0, 0.0}, {0.0, 0.002}, {0.0, 0.004}, {0.0, -0.004}, {0.0, -0.002}, },
      { {0.002, 0.0}, {0.002, 0.002}, {0.002, 0.004}, {0.002, -0.004}, {0.002, -0.002}, },
      { {0.004, 0.0}, {0.004, 0.002}, {0.004, 0.004}, {0.004, -0.004}, {0.004, -0.002}, },
      { {-0.004, 0.0}, {-0.004, 0.002}, {-0.004, 0.004}, {-0.004, -0.004}, {-0.004, -0.002}, },
      { {-0.002, 0.0}, {-0.002, 0.002}, {-0.002, 0.004}, {-0.002, -0.004}, {-0.002, -0.002}, },
  };

  // Compute frequencies and compare
  Matrix<std::pair<double, double>> freq_fftw = FFT::computeFrequencies(N, d);

  for (auto&& entry : index(freq_fftw)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);

    ASSERT_NEAR(val.first, freq_np[i][j][0], 1e-10);
    ASSERT_NEAR(val.second, freq_np[i][j][1], 1e-10);
  }

}

/*****************************************************************/
