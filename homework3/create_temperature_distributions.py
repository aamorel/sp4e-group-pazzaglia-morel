'''
Generate the desired heat source and initial temperature distributions,
plot them and save them to .csv files
'''
import numpy as np
import matplotlib.pyplot as plt
import argparse

def ind2pos(ind, n_side, side_limits):
    ''' Divide the range in n_side bins. The returned coordinate is the bin center. '''
    side_length = side_limits[1] - side_limits[0]
    return side_limits[0] + ( side_length / n_side ) * ( ind + 0.5 )

def pos_equal_to(ind, n_side, side_limits, x_target):
    ''' Return 1 if x_target falls inside the considered bin, otherwise return 0 '''
    x_coord = ind2pos(ind, n_side, side_limits)
    x_delta = ( side_limits[1] - side_limits[0] ) / n_side

    if abs(x_target - x_coord) <= (1 + 1e-9) * x_delta/2:
        return 1

    return 0

def main():
    '''
    Generate the desired heat source and initial temperature distributions,
    plot them and save them to .csv files
    '''
    ##################################
    # Parameters
    ###################################
    parser = argparse.ArgumentParser()
    parser.add_argument("n_side", help="number of particles on the side of the square", type=int)
    parser.add_argument("side_limits_low", help="low boundary value", type=float)
    parser.add_argument("side_limits_high", help="high boundary value", type=float)
    parser.add_argument("rho", help="rho", type=float)
    parser.add_argument("capacity", help="capacity", type=float)
    parser.add_argument("conductivity", help="conductivity", type=float)
    parser.add_argument("apply_boundary", help="int for applying constant temperature boundary condition, 1 True, 0 False", type=int)
    args = parser.parse_args()

    n_side = args.n_side
    side_limits = (args.side_limits_low, args.side_limits_high)

    rho = args.rho
    capacity = args.capacity
    conductivity = args.conductivity

    apply_boundary = bool(args.apply_boundary)

    # Derived parameters
    side_length = side_limits[1] - side_limits[0]

    ###################################
    # Heat source distribution
    ###################################
    heat_distribution = np.zeros((n_side, n_side))

    distribution_type = int(
        input(
            "Enter heat distribution type"
            "[0 for Null (default), 1 for sine, 2 for point sources, 3 for circular source] : "
        )
    )

    if distribution_type == 1:
        # Exercise 4.3
        aux = 2 * np.pi / side_length
        distr = lambda i, j :  aux**2 * np.sin( aux * ind2pos(j, n_side, side_limits) )

    elif distribution_type == 2:
        # Exercise 4.4
        distr = lambda i, j : (
            1 * pos_equal_to(j, n_side, side_limits, 0.5)
            - 1 * pos_equal_to(j, n_side, side_limits, -0.5)
        )

    elif distribution_type == 3:
        # Exercise 4.5
        radius = float(
        input(
            "Enter radius of circular heat source: "
        )
    )
        distr = lambda i, j : (
            1
            if ind2pos(j, n_side, side_limits)**2 + ind2pos(i, n_side, side_limits)**2 < radius**2
            else 0
        )

    else:
        # Exercise 4.2
        distr = lambda i, j : 0

    # Build distribution
    for i in range(n_side):
        for j in range(n_side):
            heat_distribution[i,j] = distr(i, j)

    ###################################
    # Initial temperature distribution
    ###################################
    temp_distribution = np.zeros((n_side, n_side))

    distribution_type = int(
        input(
            "Enter initial temperature distribution type"
            "[0 for Null (default), 1 for sine, 2 for piecewise linear] : "
        )
    )

    if distribution_type == 1:
        # Exercise 4.3
        aux = 2 * np.pi / side_length
        distr = lambda i, j :  np.sin( aux * ind2pos(j, n_side, side_limits) )

    elif distribution_type == 2:
        # Exercise 4.4
        distr = lambda i, j : (
            ( - ind2pos(j, n_side, side_limits) - 1 ) * ( ind2pos(j, n_side, side_limits) < -0.5 ) \
            + (   ind2pos(j, n_side, side_limits) )     * ( abs( ind2pos(j, n_side, side_limits) ) <= 0.5 ) \
            + ( - ind2pos(j, n_side, side_limits) + 1 ) * ( ind2pos(j, n_side, side_limits) > 0.5 )
        )

    else:
        # Exercise 4.2
        distr = lambda i, j : 0

    # Build distribution
    for i in range(n_side):
        for j in range(n_side):
            temp_distribution[i,j] = distr(i, j)

    ###################################
    # Plot distributions
    ###################################
    tick_number = 5
    tick_positions = np.linspace(0, n_side, tick_number)
    tick_labels = np.linspace(side_limits[0], side_limits[1], tick_number)

    fig = plt.figure('Heat source distribution')
    gs = plt.GridSpec(2,2)
    ax1 = fig.add_subplot(gs[:,0])
    ax2 = fig.add_subplot(gs[0,1])
    ax3 = fig.add_subplot(gs[1,1])

    ax1.imshow(heat_distribution,)
    ax1.set_title('XY Distribution')
    ax1.set_xlabel('X-axis')
    ax1.set_ylabel('Y-axis')
    ax1.set_xticks(tick_positions, tick_labels)
    ax1.set_yticks(tick_positions, tick_labels)

    ax2.set_title('X Projection')
    ax2.set_xticks(tick_positions, tick_labels)
    ax2.plot(heat_distribution)

    ax3.set_title('Y Projection')
    ax3.set_xticks(tick_positions, tick_labels)
    ax3.plot(heat_distribution.T)
    plt.tight_layout()

    fig = plt.figure('Initial temperature distribution')
    gs = plt.GridSpec(2,2)
    ax1 = fig.add_subplot(gs[:,0])
    ax2 = fig.add_subplot(gs[0,1])
    ax3 = fig.add_subplot(gs[1,1])

    ax1.imshow(temp_distribution,)
    ax1.set_title('XY Distribution')
    ax1.set_xlabel('X-axis')
    ax1.set_ylabel('Y-axis')
    ax1.set_xticks(tick_positions, tick_labels)
    ax1.set_yticks(tick_positions, tick_labels)

    ax2.set_title('X Projection')
    ax2.set_xticks(tick_positions, tick_labels)
    ax2.plot(temp_distribution)

    ax3.set_title('Y Projection')
    ax3.set_xticks(tick_positions, tick_labels)
    ax3.plot(temp_distribution.T)
    plt.tight_layout()

    plt.show()

    ###################################
    # Create csv file
    ###################################

    filename = 'temperature_field.csv'
    with open(filename, mode='w') as outfile:
        outfile.write(f'# {rho} {capacity} {conductivity} {side_length} {int(apply_boundary)}\n')

        for i in range(n_side):
            for j in range(n_side):
                outfile.write(f'{temp_distribution[i,j]} {heat_distribution[i,j]} \n')


if __name__ == '__main__':
    main()