# SP4E-group-Pazzaglia-Morel

Repository to store the solutions to the homeworks from the course SP4E (Autumn 2022).
Alessandro Pazzaglia - Aurélien Morel

# Homework 3

## Requirements
Python environment with numpy, matplotlib, pytest

C++ compiler

CMake minimum version 3.16.3

## Instructions to run the program

``` bash
cd homework3

# create temperature_field.csv
python create_temperature_distributions.py 64 -1 1 1 1 1 0 # arguments are explained below

# build the main program
mkdir build
cd build
cmake ../
make

# make dumps folder
mkdir dumps

# run main executable
./build/homework3 50000 100 temperature_field.csv material_point 0.00001 # arguments are explained below

# run visualization of results
python simulation_analysis.py 50000 100 0.00001 64 -1 1 # arguments are explained below

```

## Arguments

The python script ``create_temperature_distribution.py`` requires 7 arguments:
- n_side (int): number of particles on the side of the square
- side_limits_low (float): low boundary value
- side_limits_high (float): high boundary value
- rho (float)
- capacity (float)
- conductivity (float)
- apply_boundary (int): optionally apply constant temperature boundary condition (1 True, 0 False)

The main program requires 5 arguments:
- n_steps (int): number of steps in the simulation
- dump_freq (int): dumping frequency
- csv_file (str): file containting the initial temperature field and heat rate distribution
- particle_type (str): type of particle (material_point for temperature simulation)
- integration_step (float): integration step in seconds


The python script ``simulation_analysis.py`` requires 6 arguments:
- n_side (int): number of particles on the side of the square
- side_limits_low (float): low boundary value
- side_limits_high (float): high boundary value
- n_steps (int): number of steps in simulation
- dump_freq (int): dumping frequency
- timestep (float): integration timestep

## Explanation of the program

### create_temperature_distribution.py
This python script is used to generate the csv file that configures the simulation. It asks the user to provide the type of initial temperature and heat rate distribution that he wants.

The first line of the generated file follows the following formatting:

``# rho capacity conductivity side_length boundary``

The # is used to differentiate the first line. All the parameters can be changed in the python script. ``boundary`` is a boolean flag that is used to deal with boundary conditions when needed (explained below).

All following lines describe one particle in the grid with the format:

``initial_temperature heat_rate``

The script outputs plots to visualize the initial temperature and heat rate distribution.

### build/homework3

This is the main program, responsible to run the simulation.

If the boundary flag is set to True, we force the temperature to stay constant on the boundary of the domain. We do this by setting the delta T to zero if a particle is on the boundary.

### simulation_analysis.py

This python script is used to visualize the results from the simulation. It reads the dumps from the ``dumps`` folder and outputs an animation of the evolution of the temperature in the grid.

## Organization of the particles
Particles are created using a shared pointer and stored in a vector of shared pointers, defined as *ParticleList*. This vector is a protected member of the *System* object. We can simply access a particle using the *getParticle* method. In ``compute_temperature.cc``, we use a static cast in order to access the methods that are specific to the *Material Point* child class.

## Testing
Tests for the computation of the FFT, Inverse FFT and the Fourier frequencies are implemented in ``fft.hh``.

They can be run with the following command:
``` bash
# run tests
./build/test_fft
```

Validating tests for questions 4.2, 4.3 and 4.4 are done in python for simplicity.
They can be run with the following command:
``` bash
# run tests
pytest
```

Test 0 and 1 pass.

Test 2 is failing. We can see from our simulation analysis script that there is only a simple amplitude error on the equilibrium distribution. Due to time constraints, we did not debug this.

## Paraview

To visualize the simulation in Paraview, first we need to modify ``material_point.cc`` to include the position of each grid particle that we would need to compute in ``create_temperature_distribution.py``, in both input stream and output stream. We need to create a distribution with size 512 using the n_side parameter.
Then, from Paraview, we would load the csv files located in the ``dumps`` folder, that would contain for each particle and each dump timestep both the position and temperature. We would then color the particles based on the temperature.

