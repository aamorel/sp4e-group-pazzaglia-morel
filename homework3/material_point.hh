#ifndef __MATERIAL_POINT__HH__
#define __MATERIAL_POINT__HH__

/* -------------------------------------------------------------------------- */
#include "particle.hh"

// Class for MaterialPoint
class MaterialPoint : public Particle {
  /* ------------------------------------------------------------------------ */
  /* Methods                                                                  */
  /* ------------------------------------------------------------------------ */

public:

  // Prints the properties of the particle to the provided stream
  void printself(std::ostream& stream) const override;
  // Gets the properties of the particle from the provided stream
  void initself(std::istream& sstr) override;

  Real & getTemperature(){return temperature;};
  Real & getHeatRate(){return heat_rate;};

private:
  // Temperature associated to the particle
  Real temperature;
  // Heat rate associated to the point in space occupied by the particle
  Real heat_rate;
};

/* -------------------------------------------------------------------------- */
#endif  //__MATERIAL_POINT__HH__
