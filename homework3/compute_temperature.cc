#include "compute_temperature.hh"
#include "fft.hh"
#include "material_point.hh"
#include <cmath>

/* -------------------------------------------------------------------------- */

void ComputeTemperature::compute(System& system) {

    UInt size = system.getNbParticles();
    UInt side = std::sqrt(size);

    // Assign matrix of current particule temperatures and matrix of current heat rate

    Matrix<complex> currentTemperatures(side);
    Matrix<complex> currentHeatRate(side);

    for (int particleIndex = 0; particleIndex < size; particleIndex++) {
        int i = particleIndex % side;
        int j = particleIndex / side;
        MaterialPoint &point = static_cast<MaterialPoint&>(system.getParticle(particleIndex));

        currentTemperatures(i, j) = point.getTemperature();
        currentHeatRate(i, j) = point.getHeatRate();
    }

    // FFT of current temperatures
    Matrix<complex> currentTemperaturesF = FFT::transform(currentTemperatures);

    // FFT of current heat rate
    Matrix<complex> currentHeatRateF = FFT::transform(currentHeatRate);

    // Coordinates in the Fourier space
    Matrix<std::pair<double, double>> coordinatesF = FFT::computeFrequencies(side, sideLength/(double)side);

    // Temperature delta in the Fourier space
    Matrix<complex> deltaTemperatureF(side);
    for (auto&& entry : index(deltaTemperatureF)) {
        int i = std::get<0>(entry);
        int j = std::get<1>(entry);
        auto& val = std::get<2>(entry);
        int particleIndex = j * side + i;

        // Scale coordinates (from cycles/period to radians/period)

        double coordinate_sum = 4 * M_PI * M_PI *(
              std::pow(coordinatesF(i, j).first,  2)
            + std::pow(coordinatesF(i, j).second, 2)
        );

        // Temperature delta
        val = 1 / (rho * capacity) * (currentHeatRateF(i, j)
            - conductivity * currentTemperaturesF(i, j) * coordinate_sum);
    }

    // Temperature delta in the grid space
    Matrix<complex> deltaTemperature = FFT::itransform(deltaTemperatureF);

    // Optionally apply boundary condition
    if (boundary){
        for (int ind=0; ind<side; ind++){
            deltaTemperature(   ind,      0) = 0;
            deltaTemperature(   ind, side-1) = 0;
            deltaTemperature(     0,    ind) = 0;
            deltaTemperature(side-1,    ind) = 0;
        }
    }

    // Apply changes to particle temperatures
    for (auto&& entry : index(deltaTemperature)) {
        int i = std::get<0>(entry);
        int j = std::get<1>(entry);
        auto val = std::get<2>(entry);

        int particleIndex = j * side + i;
        MaterialPoint &point = static_cast<MaterialPoint&>(system.getParticle(particleIndex));
        point.getTemperature() += std::real(val) * timestep;
    }
}

void ComputeTemperature::setTimestep(double timestep) {
    this->timestep = timestep;
}
void ComputeTemperature::setRho(double rho) {
    this->rho = rho;
}
void ComputeTemperature::setCapacity(double capacity) {
    this->capacity = capacity;
}
void ComputeTemperature::setConductivity(double conductivity) {
    this->conductivity = conductivity;
}

void ComputeTemperature::setSideLength(double sideLength) {
    this->sideLength = sideLength;
}

void ComputeTemperature::setBoundary(bool boundary) {
    this->boundary = boundary;
}
/* -------------------------------------------------------------------------- */
