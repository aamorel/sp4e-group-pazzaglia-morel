'''
Simple script to return the initialization of a C++ array
containing the result of np.fft.fftfreq
'''
import numpy as np


def main():
    ''' Test output of numpy.fft.fftfreqs '''
    N = 5
    d = 100

    rows = np.fft.fftfreq(n= N, d= d)
    cols = np.fft.fftfreq(n= N, d= d)

    print('double freq_np[N][N][2] = {')

    for row in rows:
        print('    { ', end='')
        for col in cols:
            print('{' f'{row}, {col}' '}', end=', ')
        print('},')
    print('};')

if __name__ == '__main__':
    main()