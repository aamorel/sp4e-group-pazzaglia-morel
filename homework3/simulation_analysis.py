import csv
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import animation
import argparse

def get_distribution(step, side):

    distribution = np.zeros(side*side)
    with open(f'dumps/step-{step:05}.csv') as file:
        csvreader = csv.reader(file)
        for ind, row in enumerate(csvreader):
            if not row[0]:
                continue
            distribution[ind] = float( row[0].split()[0] )

    return distribution.reshape((side, side))

def main():
    ##################################
    # Parameters
    ###################################
    parser = argparse.ArgumentParser()
    parser.add_argument("n_steps", help="number of steps in simulation", type=int)
    parser.add_argument("dump_freq", help="dumping frequency", type=int)
    parser.add_argument("timestep", help="integration timestep", type=float)
    parser.add_argument("n_side", help="number of particles on the side of the square", type=int)
    parser.add_argument("side_limits_low", help="low boundary value", type=float)
    parser.add_argument("side_limits_high", help="high boundary value", type=float)
    args = parser.parse_args()

    n_steps = args.n_steps
    dump_freq = args.dump_freq
    timestep = args.timestep
    n_side = args.n_side
    side_limits = (args.side_limits_low, args.side_limits_high)


    ##################################
    # Plotting
    ###################################

    # Plot the evolution of the temperatures (sum, max, min)
    plt.figure('Temperature evolution')
    plt.xlabel('Time')
    plt.ylabel('Temperature')

    temp_sum = []
    temp_max = []
    temp_min = []
    times = [i * dump_freq * timestep for i in range(n_steps  // dump_freq)]
    for i in range(n_steps  // dump_freq):
        distribution = get_distribution(i * dump_freq, n_side)
        temp_sum.append( np.sum(distribution))
        temp_max.append(np.amax(distribution))
        temp_min.append(np.amin(distribution))

    temp_max_all = max(temp_max)
    temp_min_all = min(temp_min)

    plt.plot(times, temp_sum, label= 'Temperature sum')
    plt.plot(times, temp_max, label= 'Temperature max')
    plt.plot(times, temp_min, label= 'Temperature min')
    plt.legend()
    plt.grid()

    # Set up the figure, the axis, and the plot element we want to animate
    tick_number = 5
    tick_positions = np.linspace(0, n_side, tick_number)
    tick_labels = np.linspace(side_limits[0], side_limits[1], tick_number)

    fig = plt.figure('Temperature distribution')
    gs  = plt.GridSpec(2,2)

    distribution = get_distribution(0, n_side)
    tmin = temp_min_all - (temp_max_all - temp_min_all) * 0.05
    tmax = temp_max_all + (temp_max_all - temp_min_all) * 0.05

    ax1 = fig.add_subplot(gs[:,0])
    ax1.set_title('XY Distribution')
    ax1.set_xlabel('X-axis')
    ax1.set_ylabel('Y-axis')
    ax1.set_xticks(tick_positions, tick_labels)
    ax1.set_yticks(tick_positions, tick_labels)

    image_xy = ax1.imshow(distribution, vmin=tmin, vmax=tmax)
    fig.colorbar(image_xy, shrink= 0.6)

    ax2 = fig.add_subplot(gs[0,1])
    ax2.set_title('X Projection')
    ax2.set_ylim(tmin, tmax)
    ax2.grid()
    ax2.set_xticks(tick_positions, tick_labels)

    lines_x = ax2.plot(distribution)

    ax3 = fig.add_subplot(gs[1,1])
    ax3.set_title('Y Projection')
    ax3.set_ylim(tmin, tmax)
    ax3.grid()
    ax3.set_xticks(tick_positions, tick_labels)

    lines_y = ax3.plot(distribution.T)

    plt.tight_layout()

    # Animation function
    def animate(i):
        distribution = get_distribution(i * dump_freq, n_side)

        image_xy.set_array(distribution)
        for ind, (line_x, line_y) in enumerate(zip(lines_x,lines_y)):
            line_x.set_data(np.arange(n_side), distribution[ind, :])
            line_y.set_data(np.arange(n_side), distribution[:, ind])

        ax1.set_title(f'Step: {i*dump_freq} / {n_steps}')
        return [image_xy] + lines_x + lines_y

    # Call the animator
    anim = animation.FuncAnimation(
        fig,
        animate,
        frames    = n_steps  // dump_freq,
        interval  = 20,
    )

    plt.show()



if __name__ == '__main__':
    main()