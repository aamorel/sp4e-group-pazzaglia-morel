#ifndef __COMPUTE_TEMPERATURE__HH__
#define __COMPUTE_TEMPERATURE__HH__

/* -------------------------------------------------------------------------- */
#include "compute.hh"

// Compute temperature distribution evolution between material points
class ComputeTemperature : public Compute {

public:
  // Implements the algorithm for solving the 2D heat transfer equation
  void compute(System& system) override;
  // Sets the value of the timestep
  void setTimestep(double timestep);
  // Sets the value of mass density
  void setRho(double rho);
  // Sets the value of the heat capacity
  void setCapacity(double capacity);
  // Sets the value of the heat conductivity
  void setConductivity(double conductivity);
  // Sets the value of the side length
  void setSideLength(double sideLength);
  // Sets the value of the boundary variable (bool)
  void setBoundary(bool boundary);

private:
  // Timestep for the euler integration
  double timestep;
  // The mass density of the material
  double rho;
  // The heat capacity of the material
  double capacity;
  // The heat conductivity of the material
  double conductivity;
  // The side length of the material
  double sideLength;
  // Variable to apply or not the boundary condition
  bool boundary;

};

/* -------------------------------------------------------------------------- */
#endif  //__COMPUTE_TEMPERATURE__HH__
