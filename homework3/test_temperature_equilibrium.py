'''
Run tests to evaluate the implementation of the transient heat equation in 2D
'''
import subprocess
from create_temperature_distributions import ind2pos, pos_equal_to
from simulation_analysis import get_distribution
import numpy as np

error_bound = 1e-2

def test_0():
    '''
    Test with uniformly zero heat source and initial temperature distributions
    Check convergence to the known equilibrium distribution
    '''
    # Create reference equilibrium
    n_side = 64
    distr = lambda i, j : 0

    reference_distribution = np.zeros((n_side, n_side))

    for i in range(n_side):
        for j in range(n_side):
            reference_distribution[i,j] = distr(i, j)

    # Simulate to get final distribution
    subprocess.run(["./build/homework3", "50000", "100", "test_temperature_0.csv", "material_point", "0.00001"])
    end_distribution = get_distribution(49900, n_side)

    # Compare (compute norm of difference)
    error = np.linalg.norm(reference_distribution - end_distribution)
    assert error < error_bound, f"Failed test 0 with error {error}"

def test_1():
    '''
    Test with sinusoidal heat source and zero initial temperature distributions.
    Check convergence to the known equilibrium distribution
    '''
    # Create reference equilibrium
    n_side = 64
    side_length = 2
    side_limits = (-1, 1)
    aux = 2 * np.pi / side_length
    distr = lambda i, j :  np.sin( aux * ind2pos(j, n_side, side_limits) )

    reference_distribution = np.zeros((n_side, n_side))

    for i in range(n_side):
        for j in range(n_side):
            reference_distribution[i,j] = distr(i, j)

    # Simulate to get final distribution
    subprocess.run(["./build/homework3", "50000", "100", "test_temperature_1.csv", "material_point", "0.00001"])
    end_distribution = get_distribution(49900, n_side)

    # Compare (compute norm of difference)
    error = np.linalg.norm(reference_distribution - end_distribution)
    assert error < error_bound, f"Failed test 1 with error {error}"

def test_2():
    '''
    Test with kroeneker delta heat sources and zero initial temperature distributions.
    Check convergence to the known equilibrium distribution
    '''
    # Create reference equilibrium
    n_side = 64
    side_limits = (-1, 1)
    distr = lambda i, j : (
        ( - ind2pos(j, n_side, side_limits) - 1 ) * ( ind2pos(j, n_side, side_limits) < -0.5 ) \
        + (   ind2pos(j, n_side, side_limits) )     * ( abs( ind2pos(j, n_side, side_limits) ) <= 0.5 ) \
        + ( - ind2pos(j, n_side, side_limits) + 1 ) * ( ind2pos(j, n_side, side_limits) > 0.5 )
    )

    reference_distribution = np.zeros((n_side, n_side))

    for i in range(n_side):
        for j in range(n_side):
            reference_distribution[i,j] = distr(i, j)

    # Simulate to get final distribution
    subprocess.run(["./build/homework3", "50000", "100", "test_temperature_2.csv", "material_point", "0.00001"])
    end_distribution = get_distribution(49900, n_side)

    # Compare (compute norm of difference)
    error = np.linalg.norm(reference_distribution - end_distribution)
    assert error < error_bound, f"Failed test 2 with error {error}"


def main():
    ''' Run all tests '''
    test_0()
    test_1()
    test_2()


if __name__ == "__main__":
    main()