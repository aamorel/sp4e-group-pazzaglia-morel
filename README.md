# SP4E-group-Pazzaglia-Morel

Repository to store the solutions to the homeworks from the course SP4E (Autumn 2022).
Alessandro Pazzaglia - Aurélien Morel

# Homework 1

## Requirements

Create a python virtual environment.

Install the latest pip versions of:

- numpy
- scipy
- matplotlib

## Exercise 1
----------

Go to the homework1 directory:

`cd homework1`

Run the python file optimizer.py:

`python optimizer.py`

It will generate these plots:

![](homework1/images/gmres.png)
![](homework1/images/bffs.png)

## Exercise 2
----------
Go to the homework1 directory:

`cd homework1`

Run the python file gmres.py:

`python gmres.py 2 2 8 1 1 3 2 4 --solver custom --plot`

### Parameters

- a_dim_0: int

    Size of the first dimension of the A matrix

- a_dim_0: int

    Size of the second dimension of the A matrix

- ab_elements : list[float]

    List of the elements of the A matrix (a_dim_0 * a_dim_1) followed by the list of the elements of the b vector (a_dim_1)

- --solver : str, Optional

    Optionally specify the solver to be use for the GMRES iterative solver. Acceptable values are 'custom' (hand-made solver, default), 'linalg' (scipy.sparse.linalg.gmres), 'optimize' (scipy.optimize.newton_krylov).


- --plot, Optional

    Optionally plot the trajectory of convergence of the algorithm. Only for 'custom' solver. Default = False.

- --no-plot, Optional

    Do not plot the trajectory of convergence of the algorithm. Default = True


# Homework 2

## Requirements
Python environment with matplotlib
C++ compiler
CMake minimum version 3.16.3

## Instructions to run the program

``` bash
cd homework2

# build the program
mkdir build
cd build
cmake ../
make

# run main executable
./src/main 10 200 w # arguments are explained below

# run python vizualiation for exercise 3
python ../plot_results.py txt # match with input during main program

```

## Arguments

The main program requires 3 arguments:
- frequency: frequency at which series are dumped
- maxiter: max number of iterations to compute the frequency
- action: either p or w --> p will print the series in the terminal, w will write the series in a file

The python script requires 1 argument:
- filetype: either txt, csv or psv --> choose the same as you chose during main program

## Explanation of the program

The main program shows the results of each exercise as explained below:
- For exercise 2, the program asks the user to input which series he wants to compute (arithmetic or pi). It creates a shared pointer of Series object and assigns to it the ComputePi or ComputeArithmetic shared pointer. It then outputs to the terminal the result of the series computation (maxiter number of steps) using the compute method.
- For exercise 3, the program creates a DumperSeries shared pointer and then either assigns a PrintSeries or a WriteSeries shared pointer to it (user chooses as a an argument of the program). In case of WriteSeries, it also asks the user to enter the desired output file format, and then uses the setSeparator method to set the separator as needed. It then uses the dump method to either print the Series from exercise 1 to terminal or in a file.
- For exercise 4 and 5, the program shows how to use the overwritten << operator of the PrintSeries object. It creates a file and then dumps the Series of exercise 1 inside the file using the operator.
- For exercise 6, the program asks the user which function to integrate and the lower and upper limits. It then creates a RiemannIntegral object and uses the compute method to compute the results. It uses an additional function stepsForConvergence to compute when the integral reaches the expected value.

The python script plots the Series dumped in exercise 3 by the WritterSeries. It shows on the same plot the values of the Series and the expected value.

## Questions

2.1)
The best way to divide the work during this exercise was to assign to one person the role of designing the Series interface, and the other person the Dumping interface. Once the implementation of the two was completed, we further divided our work with the creation of two
branches for exercises 4,5 and for exercise 6 respectively. Merging of the exercise 6 branch
later required the management of some conflicts related to the inclusion of new methods
from exercise 5. The issue was addressed with the creation of a merge request of Gitlab.

5.1)
Supposing that the evaluation of the single Nth term of the summation has computational cost O(N^k), computing the value of the series up to the Nth term will have a complexity O(N^{k+1}).
With the current implementation the value of the series must then be evaluated for N ranging in [1 : maxiter//frequency : frequency ], leading to a global complexity of O(maxiter^{k+2}).

In the case of the Arithmetic and Pi series, the computation of a single term of the summation has cost O(1), leading to a total complexity of O(maxiter²).
This is not optimized.

5.4)
Now the complexity for dumping a series is O(N^{k+1}) since the DumperSeries adds the new terms at each new dumping time, leading to a total complexity that is the same as the one for the evaluation of the series for N = maxiter.
Note that this is only true for series whose value can be incrementally computed when adding a new summation term. This is the case for the ComputeArithmetic and ComputePi series but it is not valid for the RiemannIntegral series. Indeed, incrementing the value of N  in the Riemann Integral requires to re-compute all the elements of the summation, resulting in a complexity of O(maxiter²).

5.5)
The complexity would now be of O(maxiter^{k+2}) again since adding a new term will require to re-start the summation, leading to the same reasoning made for question 2.1.

6.4) The integrals converge to the expected values provided that we start with a number of sub-intervals greater than 2. Starting with a single sub-interval in the case of the cosine integral immediately leads to the correct solution (0.00), but this is due to a pure coincidence rather than to a convergence of the algorithm. A more robust way of solving the integral might be to also evaluate the gradient of the obtained approximations to check when it finally settles to close-to-zero values.
The convergence for the three integrals was obtained with:

- Cubic function: Minimum N = 50
- Cosine function: Minimum N = 315
- Sine function: Minimum N = 79

- Cubic function: Minimum N = 50
- Cosine function: Minimum N = 315
- Sine function: Minimum N = 79